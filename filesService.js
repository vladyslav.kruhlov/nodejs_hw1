const fs = require('fs');
const path = require('path');

function createFile(req, res, next) {
    try {
        if (!req.body.filename) {
            res.status(400).send({
                'message': 'Please specify \'filename\' parameter'
            })
        }
        if (!req.body.content && req.body.content !== '') {
            res.status(400).send({
                'message': 'Please specify \'content\' parameter'
            })
        }
        const allAllowedExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
        const ext = req.body.filename ? path.extname(req.body.filename) : undefined
        const password = req.body.password || undefined
        const content = req.body.content === '' ? '' : req.body.content

        if (!ext) {
            res.status(400).send({
                'message': 'Please specify correctly \'filename\' parameter'
            })
        }
        if (!allAllowedExtensions.includes(ext.toString())) {
            res.status(400).send({
                'message': 'Please enter file with following extension log, txt, json, yaml, xml, js'
            })
        }
        if (password) {
            const data = JSON.parse(fs.readFileSync('passwords.json', (_, data) => data))

            fs.writeFileSync('passwords.json', JSON.stringify([
                ...data,
                {[req.body.filename]: password}
            ]))
        }

        fs.writeFileSync(path.join('files', req.body.filename), content)

        res.status(200).send({
            'message': 'File created successfully'
        })
    } catch (err) {
        next(err)
    }
}

function getFiles(req, res, next) {
    try {
        const files = fs.readdirSync('files')

        res.status(200).send({
            'message': 'Success',
            'files': files,
        });
    } catch (err) {
        next(err)
    }
}

const getFile = (req, res, next) => {
    try {
        const filename = req.url.replace('/', '')

        const {password} = req.query
        const allPasswords = JSON.parse(fs.readFileSync('passwords.json', (err, data) => data))
        let requirePass = false
        let passValidation = false

        for (const file of allPasswords) {
            if (file[filename]) {
                requirePass = true

                if (file[filename] === password) {
                    passValidation = true
                }
            }
        }

        if (!requirePass || requirePass && passValidation) {
            const data = fs.readFileSync(path.join('files', filename), {
                encoding: 'utf8',
                flag: 'r'
            })
            const {birthtime} = fs.statSync(path.join('files', filename))

            res.status(200).send({
                'message': 'Success',
                'filename': filename,
                'content': data,
                'extension': path.extname(filename).replace('.', ''),
                'uploadedDate': birthtime
            });
        }
        if (requirePass && !passValidation) {
            res.status(400).send({
                'message': 'Password is not correct. Please enter correct password'
            })

        }
    } catch (err) {
        res.status(400).send({
            'message': 'No file with \'notes.txt\' filename found'
        })
    }
}

const deleteFile = (req, res, next) => {
    try {
        const filename = req.url.replace('/', '') || undefined
        const allFiles = fs.readdirSync('files').map(filename => filename)

        if (!allFiles.includes(filename)) {
            res.status(400).send({
                'message': 'cannot find such a file to delete it'
            })
        }

        fs.unlinkSync(path.join('./files', filename))

        res.status(200).send({
            'message': 'successfully deleted'
        })
    } catch (err) {
        next(err)
    }
}

const editFile = (req, res, next) => {
    try {
        const filename = req.url.replace('/', '')
        const content = req.body.content
        const allFiles = fs.readdirSync('files').map(filename => filename)

        if (!allFiles.includes(filename)) {
            res.status(400).send({
                'message': 'cannot find such a file to modify it'
            })
        }

        fs.writeFileSync(path.join('files', filename), content)

        res.status(200).send({
            'message': 'successfully edited'
        })
    } catch (err) {
        next(err)
    }
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    deleteFile,
    editFile
}
